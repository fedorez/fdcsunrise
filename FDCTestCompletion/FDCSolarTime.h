//
//  FDCSolarTime.h
//  FDCTestCompletion
//
//  Created by Denis Fedorets on 16.04.17.
//  Copyright © 2017 Denis Fedorets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface FDCSolarTime : NSObject

@property (nonatomic) NSDate* sunrise;
@property (nonatomic) NSDate* sunset;
@property (nonatomic) NSDate* solar_noon;
@property (nonatomic) NSDate* day_length;
@property (nonatomic) NSDate* civil_twilight_begin;
@property (nonatomic) NSDate* civil_twilight_end;
@property (nonatomic) NSDate* nautical_twilight_begin;
@property (nonatomic) NSDate* nautical_twilight_end;
@property (nonatomic) NSDate* astronomical_twilight_begin;
@property (nonatomic) NSDate* astronomical_twilight_end;
@property (atomic) BOOL isDefined;

- (instancetype)initWithCoordsLat:(CLLocationDegrees)lat Lon:(CLLocationDegrees)lon andDate:(NSDate *)interestDate;

@end
