//
//  FDCAddressClass.h
//  FDCTestCompletion
//
//  Created by Denis Fedorets on 30.03.17.
//  Copyright © 2017 Denis Fedorets. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface FDCAddressClass : NSObject{
    CLLocationDegrees m_lat;
    CLLocationDegrees m_lon;
}

@property (nonatomic) NSString* address;
@property (nonatomic) NSString* name;
@property (nonatomic) NSString* coordinates;
@property (nonatomic, readonly) CLLocationDegrees lat;
@property (nonatomic, readonly) CLLocationDegrees lon;

- (instancetype)initWithAddress:(NSString *)newAddress name:(NSString *)name andCoordinates:(NSString *)newCoordinates;

@end
