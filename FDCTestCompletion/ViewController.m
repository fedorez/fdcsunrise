//
//  ViewController.m
//  FDCTestCompletion
//
//  Created by Denis Fedorets on 25.03.17.
//  Copyright © 2017 Denis Fedorets. All rights reserved.
//

#import "ViewController.h"

@interface MyLayoutGuide : NSObject <UILayoutSupport>
@property (nonatomic) CGFloat length;
-(id)initWithLength:(CGFloat)length;
@end

@implementation MyLayoutGuide
@synthesize length = _length;
@synthesize topAnchor = _topAnchor;
@synthesize bottomAnchor = _bottomAnchor;
@synthesize heightAnchor = _heightAnchor;

- (id)initWithLength:(CGFloat)length
{
    if (self = [super init]) {
        _length = length;
    }
    return self;
}

@end

@interface ViewController ()
//- (NSData *)getServerResponceForPattern: (NSString *)stringPattern;
- (void)reloadAutocompleteUsingData:(NSData *) responseData;
- (void) obtainServerResponceForPattern: (NSString *)stringPattern;
- (void) redrawMapViewRectWithKeyboardHeight: (CGFloat)keyboardHeight;
@property (atomic) CGFloat actualKeyboardHeight;

@end

@implementation ViewController

static void *myContext = &myContext;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    txtAddress = [[UITextField alloc] initWithFrame:CGRectMake(10.0f, screenRect.origin.y+40.0f, screenRect.size.width-20.0f, 40.0f)];
    txtAddress.text = @"";
    txtAddress.backgroundColor = [UIColor yellowColor];
    txtAddress.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:txtAddress];
    
    //Observing changes to myUITextField.text:
    [txtAddress addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    CGRect txtRect = [txtAddress bounds];
    
    _mapView = [[MKMapView alloc] initWithFrame:
                CGRectMake(10, screenRect.origin.y+40.0f+txtRect.size.height+10, txtRect.size.width, screenRect.size.height-40.0f-10.0f-txtRect.size.height-10.0f)];
    _mapView.layer.cornerRadius = 5.0f;
    [self.view addSubview:_mapView];
    self.mapView.delegate = self;
    
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleGesture:)];
    tgr.numberOfTapsRequired = 1;
    tgr.numberOfTouchesRequired = 1;
    [self.mapView addGestureRecognizer:tgr];
    
    autocompleteTableView = [[UITableView alloc] initWithFrame:
                             CGRectMake(10, screenRect.origin.y+40.0f+txtRect.size.height+10, txtRect.size.width, txtRect.size.width) style:UITableViewStylePlain];
    autocompleteTableView.delegate = self;
    autocompleteTableView.dataSource = self;
    autocompleteTableView.scrollEnabled = YES;
    
    autocompleteTableView.hidden = YES;
    [self.view addSubview:autocompleteTableView];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    
    self.actualKeyboardHeight = 0.0f;
}

- (id)bottomLayoutGuide
{
    CGFloat bottomLayoutGuideLength = 50.f;
    
    return [[MyLayoutGuide alloc] initWithLength:bottomLayoutGuideLength];
}

- (void)updateBottomLayoutGuides
{
    // this method ends up calling -(id)bottomLayoutGuide on its UIViewController
    // and thus updating where the Legal link on the map should be.
    [self.mapView setNeedsLayout];
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer
{
    [self->txtAddress resignFirstResponder];
}

- (void) orientationChanged:(NSNotification *)note
{
    NSLog(@"Orientation changed!");
    
    //let's change the sizes
    [self redrawMapViewRectWithKeyboardHeight:self.actualKeyboardHeight];
    
    
 //   if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)){
 //       //Do something in landscape
 //   }
 //   else {
 //       //Do something in portrait
 //   }
}

- (void) redrawMapViewRectWithKeyboardHeight: (CGFloat)keyboardHeight {
    //let's change the sizes
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    CGRect txtRect = CGRectMake(10.0f, screenRect.origin.y+40.0f, screenRect.size.width-20.0f, 40.0f);
    [self->txtAddress setFrame:txtRect];
    
    CGRect mapViewRect = CGRectMake(10, screenRect.origin.y+40.0f+txtRect.size.height+10, txtRect.size.width, screenRect.size.height-40.0f-10.0f-txtRect.size.height-10.0f-keyboardHeight);
    [self.mapView setFrame:mapViewRect];
    
    CGRect autocompleteTableViewRect = CGRectMake(10, screenRect.origin.y+40.0f+txtRect.size.height+10, txtRect.size.width, txtRect.size.width);
    [self->autocompleteTableView setFrame:autocompleteTableViewRect];
}

-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog( @"text changed: %@", theTextField.text);
    //Fire timer
    [timerAutoComplete invalidate];
    timerAutoComplete = nil;
    timerAutoComplete = [NSTimer scheduledTimerWithTimeInterval:0.3f
                                                      target:self
                                                    selector:@selector(reactForTimer:)
                                                    userInfo:nil
                                                     repeats:NO];
    
}

- (void)reactForTimer:(NSTimer *)timer {
    [timerAutoComplete invalidate];
    timerAutoComplete = nil;
    NSLog( @"timer tick!");
    [self reactionForNewTextValueInAddressTextField: self->txtAddress.text];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self->arrCompleteItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    //Поиск ячейки
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //Если ячейка не найдена
    if (cell == nil) {
        //Создание ячейки
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                       reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [[self->arrCompleteItems objectAtIndex:indexPath.row] name];
    cell.detailTextLabel.text = [[self->arrCompleteItems objectAtIndex:indexPath.row] address];
    cell.tag = indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    self->selectedText = selectedCell.textLabel.text;
    self->selectedAddress = [self->arrCompleteItems objectAtIndex:selectedCell.tag];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveSolarDataNotification:)
                                                 name:@"FDCDownloadSolarDataNotification"
                                               object:nil];
    
    self->selectedSolarData = [[FDCSolarTime alloc] initWithCoordsLat:self->selectedAddress.lat Lon:self->selectedAddress.lon andDate:[NSDate date]];
    [self goPressed];
    
}

- (void) receiveSolarDataNotification:(NSNotification *) notification
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YY-MM-dd hh:mm:ss a"];
    NSString *dateString = [dateFormatter stringFromDate:selectedSolarData.sunrise];
    if ([[self.mapView annotations][0] isKindOfClass:[MKPointAnnotation class]])
    {
        MKPointAnnotation *pa = (MKPointAnnotation *)[self.mapView annotations][0];
        
        NSString *emojiRisingSun = MyStringFromUnicodeCharacter(0x1F305);
        
        pa.subtitle =[NSString stringWithFormat:@"%@ %@",emojiRisingSun, dateString];
    }
}

NSString *MyStringFromUnicodeCharacter(uint32_t character) {
    uint32_t bytes = htonl(character); // Convert the character to a known ordering
    return [[NSString alloc] initWithBytes:&bytes length:sizeof(uint32_t) encoding:NSUTF32StringEncoding];
}

- (IBAction)goPressed {
    
    // Clean up UI
    autocompleteTableView.hidden = YES;
    
    // Tell the web view controller where to go
    txtAddress.text = self->selectedText;
    
    //Just show pin on the map
    //FDCAddressClass currAddress = [arrCompleteItems objectAtIndex:1];
    NSLog(@"%f", selectedAddress.lat);
    NSLog(@"%f", selectedAddress.lon);
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(selectedAddress.lat, selectedAddress.lon);
    
    MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
    MKCoordinateRegion region = {coord, span};
    
    if ([self.mapView annotations].count>0) {
        [self.mapView removeAnnotation:[self.mapView annotations][0]];
    }
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coord];
    annotation.title = selectedAddress.name;
    
    [self.mapView setRegion:region animated:YES];
    [self.mapView addAnnotation:annotation];
    //remove focus - hide keyboard
    [self->txtAddress resignFirstResponder];
}


- (void)reactionForNewTextValueInAddressTextField:(NSString *)newText {
    if (newText.length>0) {
        [self reloadAutocompleteForString:newText];
        autocompleteTableView.hidden = NO;
        
    } else {
        autocompleteTableView.hidden = YES;
    }
}

- (void)reloadAutocompleteForString:(NSString *)stringPattern {
    NSLog( @"query for pattern: %@", stringPattern);
    
    [self obtainServerResponceForPattern: stringPattern];
}

- (void)reloadAutocompleteUsingData:(NSData *) responseData {
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    if ([NSJSONSerialization isValidJSONObject:responseDic])
    {
        NSString *key_response = @"response";
        NSString *key_GeoObjectCollection = @"GeoObjectCollection";
        NSString *key_featureMember = @"featureMember";
        NSString *key_GeoObject = @"GeoObject";
        NSString *key_metaDataProperty = @"metaDataProperty";
        NSString *key_GeocoderMetaData = @"GeocoderMetaData";
        NSString *key_text = @"text";
        NSString *key_point=@"Point";
        NSString *key_pos=@"pos";
        NSString *key_name=@"name";
        
        NSArray *array = [[[responseDic objectForKey:key_response] objectForKey:key_GeoObjectCollection] objectForKey:key_featureMember];
        NSMutableArray *completeItems = [NSMutableArray new];
        [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            NSString *currAddress=[[[[obj objectForKey:key_GeoObject] objectForKey:key_metaDataProperty] objectForKey:key_GeocoderMetaData] objectForKey:key_text];
            NSString *currPoint = [[[obj objectForKey:key_GeoObject] objectForKey:key_point] objectForKey:key_pos];
            
            NSString *currName = [[obj objectForKey:key_GeoObject] objectForKey:key_name];
            
            [completeItems addObject:[[FDCAddressClass alloc] initWithAddress:currAddress name:currName andCoordinates:currPoint]];
            
        }];
        
        NSLog(@"%@", completeItems);
        
        self->arrCompleteItems = completeItems;
    }
    
    [autocompleteTableView reloadData];
}

//- (NSData *)getServerResponceForPattern: (NSString *)stringPattern {
//    
//    NSString *addressUriString = [NSString stringWithFormat:@"%@%@", @"https://geocode-maps.yandex.ru/1.x/?format=json&geocode=", stringPattern];
//    
//    NSString* webStringURL = [addressUriString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//    NSURL* url = [NSURL URLWithString:webStringURL];
//    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    NSURLResponse * response;
//    NSError * error;
//    
//    NSData * data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    return data;
//}

- (void) obtainServerResponceForPattern: (NSString *)stringPattern {
    NSString *addressUriString = [NSString stringWithFormat:@"%@%@", @"https://geocode-maps.yandex.ru/1.x/?format=json&geocode=", stringPattern];
    
    //NSString* webStringURL = [addressUriString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];// deprecated >=iOS9
    NSString* webStringURL = [addressUriString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL* url = [NSURL URLWithString:webStringURL];
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url
                                                         completionHandler:
                              ^(NSData *data, NSURLResponse *response, NSError *error) {
                                  if (data) {
                                      // Do stuff with the data
                                      [self performSelectorOnMainThread:@selector(reloadAutocompleteUsingData:) withObject:data waitUntilDone:YES];
                                  } else {
                                      NSLog(@"Failed to fetch %@: %@", url, error);
                                  }
                              }];
    [task resume];
}

- (void) viewWillAppear:(BOOL)paramAnimated{
    [super viewWillAppear:paramAnimated];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleKeyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleKeyboardWillHide:)
     name:UIKeyboardWillHideNotification object:nil];    
}

- (void) handleKeyboardDidShow:(NSNotification *)paramNotification{
    
    NSValue *keyboardRectAsObject =
    [[paramNotification userInfo]
     objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    
    CGRect keyboardRect = CGRectZero;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    _actualKeyboardHeight = keyboardRect.size.height;
    
    [self redrawMapViewRectWithKeyboardHeight:_actualKeyboardHeight];
}

- (void) handleKeyboardWillHide:(NSNotification *)paramNotification{
    _actualKeyboardHeight = 0.0f;
    [self redrawMapViewRectWithKeyboardHeight:_actualKeyboardHeight];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


@end
