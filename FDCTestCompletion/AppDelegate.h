//
//  AppDelegate.h
//  FDCTestCompletion
//
//  Created by Denis Fedorets on 25.03.17.
//  Copyright © 2017 Denis Fedorets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

