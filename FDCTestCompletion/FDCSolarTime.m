//
//  FDCSolarTime.m
//  FDCTestCompletion
//
//  Created by Denis Fedorets on 16.04.17.
//  Copyright © 2017 Denis Fedorets. All rights reserved.
//

#import "FDCSolarTime.h"

@interface FDCSolarTime ()

- (void) obtainServerResponceForLat: (CLLocationDegrees)lat andLon:(CLLocationDegrees)lon andDate:(NSDate *)interestDate;

@end

@implementation FDCSolarTime

@synthesize sunrise;
@synthesize sunset;
@synthesize solar_noon;
@synthesize day_length;
@synthesize civil_twilight_begin;
@synthesize civil_twilight_end;
@synthesize nautical_twilight_begin;
@synthesize nautical_twilight_end;
@synthesize astronomical_twilight_begin;
@synthesize astronomical_twilight_end;
@synthesize isDefined;

- (instancetype)initWithCoordsLat:(CLLocationDegrees)lat Lon:(CLLocationDegrees)lon andDate:(NSDate *)interestDate{
    self = [super init];
    self.isDefined = NO;
    
    [self obtainServerResponceForLat:lat andLon:lon andDate:interestDate];
    return self;
}

- (void) obtainServerResponceForLat: (CLLocationDegrees)lat andLon:(CLLocationDegrees)lon andDate:(NSDate *)interestDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YY-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:interestDate];
    
    NSString *addressUriString = [NSString stringWithFormat:@"https://api.sunrise-sunset.org/json?lat=%f&lng=%f&date=%@", lat, lon, dateString];
    
    NSString* webStringURL = [addressUriString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL* url = [NSURL URLWithString:webStringURL];
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url
                                                         completionHandler:
                              ^(NSData *data, NSURLResponse *response, NSError *error) {
                                  if (data) {
                                      // Do stuff with the data
                                      [self performSelector:@selector(completeFieldsUsingData:forDate:) withObject:data withObject:interestDate];
                                  } else {
                                      NSLog(@"Failed to fetch %@: %@", url, error);
                                  }
                              }];
    [task resume];
}

- (void)completeFieldsUsingData:(NSData *) responseData forDate:(NSDate *)interestDate{
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    if ([NSJSONSerialization isValidJSONObject:responseDic])
    {
        
        NSString *key_results = @"results";
        NSString *key_sunrise = @"sunrise";
        NSString *key_sunset = @"sunset";
        //NSString *key_solar_noon = @"solar_noon";
        //NSString *key_day_length = @"day_length";
        NSString *key_civil_twilight_begin = @"civil_twilight_begin";
        NSString *key_civil_twilight_end = @"civil_twilight_end";
        NSString *key_astronomical_twilight_begin=@"astronomical_twilight_begin";
        NSString *key_astronomical_twilight_end=@"astronomical_twilight_end";
        NSString *key_status=@"status";
        
        if ([[responseDic objectForKey:key_status] isEqualToString:@"OK"]){
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"YY-MM-dd hh:mm:ss a"];
            [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
            [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
            
            NSDateFormatter *newFormatter = [[NSDateFormatter alloc] init];
            [newFormatter setDateFormat:@"YY-MM-dd"];
            NSString *dateString = [newFormatter stringFromDate:interestDate];
            
            NSString *sunriseDateString = [[responseDic objectForKey:key_results] objectForKey:key_sunrise];
            NSDate *sunriseDate = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@",dateString,sunriseDateString]];
            self->sunrise=sunriseDate;
            
            NSString *sunsetDateString = [[responseDic objectForKey:key_results] objectForKey:key_sunset];
            NSDate *sunsetDate = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@",dateString,sunsetDateString]];
            self->sunset=sunsetDate;
            
            NSString *civil_twilight_beginDateString = [[responseDic objectForKey:key_results] objectForKey:key_civil_twilight_begin];
            NSDate *civil_twilight_beginDate = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@",dateString,civil_twilight_beginDateString]];
            self->civil_twilight_begin=civil_twilight_beginDate;
            
            NSString *civil_twilight_endDateString = [[responseDic objectForKey:key_results] objectForKey:key_civil_twilight_end];
            NSDate *civil_twilight_endDate = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@",dateString,civil_twilight_endDateString]];
            self->civil_twilight_end=civil_twilight_endDate;
            
            NSString *astronomical_twilight_beginDateString = [[responseDic objectForKey:key_results] objectForKey:key_astronomical_twilight_begin];
            NSDate *astronomical_twilight_beginDate = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@",dateString,astronomical_twilight_beginDateString]];
            self->astronomical_twilight_begin=astronomical_twilight_beginDate;
            
            NSString *astronomical_twilight_endDateString = [[responseDic objectForKey:key_results] objectForKey:key_astronomical_twilight_end];
            NSDate *astronomical_twilight_endDate = [dateFormat dateFromString:[NSString stringWithFormat:@"%@ %@",dateString,astronomical_twilight_endDateString]];
            self->astronomical_twilight_end=astronomical_twilight_endDate;
            
            self->isDefined = YES;
        }
        else{
            self->isDefined = NO;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FDCDownloadSolarDataNotification" object:self];
        
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
