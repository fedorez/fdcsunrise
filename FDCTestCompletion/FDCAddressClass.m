//
//  FDCAddressClass.m
//  FDCTestCompletion
//
//  Created by Denis Fedorets on 30.03.17.
//  Copyright © 2017 Denis Fedorets. All rights reserved.
//

#import "FDCAddressClass.h"

@implementation FDCAddressClass

@synthesize address;
@synthesize coordinates;

- (instancetype)initWithAddress:(NSString *)newAddress name:(NSString *)name andCoordinates:(NSString *)newCoordinates{
    self = [super init];
    if (self) {
        self.address = newAddress;
        self.name = name;
        self.coordinates = newCoordinates;
        NSArray *items = [self.coordinates componentsSeparatedByString:@" "];
        m_lon = [[items objectAtIndex:0] doubleValue];
        m_lat = [[items objectAtIndex:1] doubleValue];
    }
    return self;
}

- (CLLocationDegrees) lat{
    return m_lat;
}

- (CLLocationDegrees) lon{
    return m_lon;
}

@end
