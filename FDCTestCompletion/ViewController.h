//
//  ViewController.h
//  FDCTestCompletion
//
//  Created by Denis Fedorets on 25.03.17.
//  Copyright © 2017 Denis Fedorets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "FDCAddressClass.h"
#import "FDCSolarTime.h"

@interface ViewController : UIViewController{

    IBOutlet UITextField *txtAddress;
    UITableView *autocompleteTableView;
    NSMutableArray *arrCompleteItems;
    NSString *selectedText;
    FDCAddressClass *selectedAddress;
    NSTimer *timerAutoComplete;
    FDCSolarTime *selectedSolarData;
}

@property (nonatomic, strong) IBOutlet MKMapView *mapView;

- (void)reloadAutocompleteForString:(NSString *)stringPattern;

@end

